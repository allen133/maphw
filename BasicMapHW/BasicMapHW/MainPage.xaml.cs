﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Collections.ObjectModel;


namespace BasicMapHW
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Pin> pinNames;
        public MainPage()
        {
            InitializeComponent();

            var initialMapLocation = MapSpan.FromCenterAndRadius(
                new Position(33.1307785, -117.1601826),
                Distance.FromMiles(1));

            MyMap.MoveToRegion(initialMapLocation);

            PopulatePicker();
        }

        void changeStreet(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Street;
        }

        void changeSate(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Satellite;
        }

        void changeHybrid(object sender, EventArgs e)
        {
            MyMap.MapType = MapType.Hybrid;
        }

        void PopulatePicker()
        {
            pinNames = new ObservableCollection<Pin>
            {
                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Sushi Yama",
                    Address = "500 N Escondido Blvd #2628, Escondido, CA 92025",
                    Position = new Position(33.1289923,-117.1400227)
                },
                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Poki Poki",
                    Address = "1348 W Valley Pkwy suite f, Escondido, CA 92029",
                    Position = new Position(33.1122976,-117.1028971)
                },
                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Cheese Cake Factory",
                    Address = "200 E Via Rancho Pkwy Ste 370, Escondido, CA 92025",
                    Position = new Position(33.07074,-117.0662147)
                },
                new Pin
                {
                    Type = PinType.SavedPin,
                    Label = "Yogurtland",
                    Address = "1284-A Auto Park Way, Escondido, CA 92029",
                    Position = new Position(33.113265,-117.101293)
                }
            };

            PinPicker.Items.Add("Favorite Places to Eat");
            foreach (Pin item in pinNames)
            {
                //Load each pin into the map and picker
                PinPicker.Items.Add(item.Label);
                MyMap.Pins.Add(item);
            }

            PinPicker.SelectedIndex = 0;
        }

        private void Handle_SelectedPinChange(object sender, System.EventArgs e)
        {
            var inputSlot = (Picker)sender;
            string pname = inputSlot.SelectedItem.ToString();
            //When user selects a pin name, find the pin
            foreach (Pin item in pinNames)
            {
                if (item.Label == pname)
                {
                    //Move the map to center on the pin
                    MyMap.MoveToRegion(MapSpan.FromCenterAndRadius(item.Position, Distance.FromMiles(1)).WithZoom(19));
                }
            }
        }

    }
}
